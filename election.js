const http = require('http');
const fs = require('fs');
const os = require('os');

const hostname = os.hostname();
const port = 2020;

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var url = 'https://politics-elex-results.data.api.cnn.io/results/view/2020-national-races-PG.json';
var urlSenate = 'https://politics-elex-results.data.api.cnn.io/results/view/2020-national-races-SG.json';

var margin = 0.001;
var payload = [];
var last = [];

var payloadSenate = [];
var lastSenate = [];


refreshDataSenate();
setInterval(refreshDataSenate, 1000 * 10);

refreshData();
setInterval(refreshData, 1000 * 10);

function refreshData() {
    payload = [];
    var req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.setRequestHeader("Content-type", "text/html");
    req.setRequestHeader('Accept', 'text/html');
    req.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var data = JSON.parse(req.responseText);
            var states = ['PA', 'NC', 'AZ', 'NV', 'GA', 'AK'];
            for (var i = 0; i < data.length; i++) {
                if (states.includes(data[i].stateAbbreviation)) {
                    if (data[i].stateAbbreviation == 'NC') data[i].stateName = 'N. Carolina';
                    if (data[i].stateAbbreviation == 'NV') data[i].stateName = 'Nevada  ';
                    var ecv = 0;
                    switch (data[i].stateAbbreviation) {
                        case 'NV': ecv = 6; break;
                        case 'NC': ecv = 15;break;
                        case 'AZ': ecv = 11;break;
                        case 'GA': ecv = 16;break;
                        case 'PA': ecv = 20;break;
                        case 'AK': ecv = 3;break;
                    }

                    var vt = 0;
                    var vb = 0;
                    var pr = data[i].percentReporting;
                    for (var k = 0; k < data[i].candidates.length; k++) {
                        if (data[i].candidates[k].lastName == 'Trump')
                            vt = data[i].candidates[k].voteNum;
                        if (data[i].candidates[k].lastName == 'Biden')
                            vb = data[i].candidates[k].voteNum;
                    }
                    var leftovervotes = Math.floor((vb + vt) / pr * 100) - vb - vt;
                    var bidenneedstowin = ((((vb - vt) - (vb+vt+leftovervotes) * margin) / leftovervotes - 1 ) / -2) * 100;
                    payload.push([data[i].stateName, vt, vb, pr, leftovervotes, Math.ceil(bidenneedstowin*100)/100, ecv]);
                    console.log(data[i].stateName + " \tT: " +vt + "\tB: " + vb + "\tAlready counted " + pr + "%\tLeftover " + leftovervotes + "\tBiden needs: "+ Math.ceil(bidenneedstowin*100)/100 + "%");
                }
            }
            
        }
        


        last = payload;
    }
    req.send();
    console.log("---------------------------------------What Biden needs to win by a "+margin*100+"% margin-------------------------------------------------");
}

function refreshDataSenate() {
    payloadSenate = [];
    var reqSenate = new XMLHttpRequest();
    reqSenate.open("GET", urlSenate, true);
    reqSenate.setRequestHeader("Content-type", "text/html");
    reqSenate.setRequestHeader('Accept', 'text/html');
    reqSenate.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var data = JSON.parse(reqSenate.responseText);
            var states = ['AZ', 'AK', 'GA', 'NC', 'ME'];
            for (var i = 0; i < data.length; i++) {
                if (states.includes(data[i].stateAbbreviation)) {
                    if (data[i].stateAbbreviation == 'NC') data[i].stateName = 'N. Carolina';
                    var seats = 0;
                    switch (data[i].stateAbbreviation) {
                        default: seats = 1;break;
                    }

                    var rep = 0;
                    var dem = 0;
                    var pr = data[i].percentReporting;
                    for (var k = 0; k < data[i].candidates.length; k++) {
                        if (data[i].candidates[k].majorParty == 'REP')
                            rep = data[i].candidates[k].voteNum;
                        if (data[i].candidates[k].majorParty == 'DEM')
                            dem = data[i].candidates[k].voteNum;
                    } 
                    var leftovervotes = Math.floor((rep + dem) / pr * 100) - rep - dem;
                    var demsneedtowin = ((((dem - rep) - (dem+rep+leftovervotes) * margin) / leftovervotes - 1 ) / -2) * 100;
                    payloadSenate.push([data[i].stateName, rep, dem, pr, leftovervotes, Math.ceil(demsneedtowin*100)/100, seats]);
                }
            }
            
        }
        lastSenate = payloadSenate;
    }
    reqSenate.send();
}

const server = http.createServer((req, res) => {
    var url = req.url;
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:'+port);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    console.log(`${req.connection.remoteAddress}: ${req.method} - ${url}`);

    if (req.method == 'GET') {
        if (url === '/') { // gibt frontend seite zurück
            res.setHeader('Content-Type', 'text/html');
            fs.readFile('./index.html', (err, d) => {
                if (err) {
                    res.statusCode = 404;
                    res.end(err.message);
                } else {
                    res.statusCode = 200;
                    res.end(d);
                }
            });
        } else if (url === '/alert') { // gibt audio datei zurück
            res.setHeader('Content-Type', 'audio/wav');
            fs.readFile('./alert.wav', (err, d) => {
                if (err) {
                    res.statusCode = 404;
                    res.end(err.message);
                } else {
                    res.statusCode = 200;
                    res.end(d);
                }
            });
        } else if(url === '/data') {
            res.setHeader('Content-Type', 'text/plain');
            res.statusCode = 200;
            res.end(JSON.stringify(payload));
        } else if(url === '/dataSenate') {
            res.setHeader('Content-Type', 'text/plain');
            res.statusCode = 200;
            res.end(JSON.stringify(payloadSenate));
        } else {
            res.statusCode = "501";
            res.end("Server-Error: Requested-Url-Not-Supported");
        }
    } else if (req.method == 'POST') {
        res.statusCode = "501";
        res.end("Server-Error: Requested-Url-Not-Supported");
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});